import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {loadUser} from "./redux/actions/accounts/login";
import {connect} from "react-redux";
import configureStore from "./redux/Store";
import {logoutAction} from "./redux/actions/accounts/login";
import PropTypes from "prop-types";
import 'bootswatch/dist/cosmo/bootstrap.min.css';
import {joinStateResetAction} from "./redux/actions/join/join";
import {statusStateResetAction} from "./redux/actions/status/status";
import {payStateResetAction} from "./redux/actions/pay/pay";
import {saleBuyStateResetAction} from "./redux/actions/sale_buy/sale_buy";

class Header extends Component {

    static propType={
        joinStateResetAction: PropTypes.func,
        statusStateResetAction: PropTypes.func,
        payStateResetAction: PropTypes.func,
        saleBuyStateResetAction: PropTypes.func
    };
    logoutHandler=()=>{
        const {payStateResetAction, joinStateResetAction, statusStateResetAction, saleBuyStateResetAction} = this.props;
        payStateResetAction();
        joinStateResetAction();
        statusStateResetAction();
        saleBuyStateResetAction();
        this.props.logoutAction();

    };
    static propTypes={
      isAuthenticated: PropTypes.bool,
      logoutAction:PropTypes.func,
        login:PropTypes.object.isRequired,
    };

    render() {
        const userLink=(
            <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <Link className="nav-link" to="/signup">
                                    SignUp
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/login">
                                    Login
                                </Link>
                            </li>
                        </ul>
        );
        const {isAuthenticated,user}=this.props.login;
        const guestLink=(
            <ul className="navbar-nav ml-auto">
                           <li className="nav-item">
                              <span className="navbar-text mr-3">
                              <strong>
                                  {user?`welcome ${user.first_name}`:''}
                              </strong>
                              </span>
                           </li>
                            <li className="nav-item">
                                <button className="nav-link btn btn-info btn-sm text-white" onClick={this.logoutHandler}>Logout</button>
                            </li>
                        </ul>
        );
        return (
            <div>
                {console.log(isAuthenticated,'from the return')}
                <nav className="navbar navbar-expand-sm navbar-light bg-light">
                    <div className="container">
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                        <a className="navbar-brand" href="#">Ze-Equb</a>
                        <ul className="navbar-nav mr-auto mt-2 mt-lg-0 text-sm-right">
                        </ul>
                    </div>
                    <div className="navbar-collapse collapse w-100 order-3 dual-collapse2">
                        {
                            !isAuthenticated ? userLink:guestLink
                        }
                    </div>
                    </div>
                </nav>
            </div>
        );
    }
}
const mapStateToProps=state=>({
   isAuthenticated:state.login.isAuthenticated,
    login:state.login
});

export default  connect(mapStateToProps,{logoutAction, joinStateResetAction, payStateResetAction, statusStateResetAction, saleBuyStateResetAction})(Header);