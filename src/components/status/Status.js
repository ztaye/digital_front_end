import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {statusGetWinnersAction} from "../redux/actions/status/status";
import StatusDetails from "./StatusDetails";
import StatusDetail from "./StatusDetail";
class Status extends Component {

    static propType={
        status:PropTypes.array,
        statusGetWinnersAction:PropTypes.func,
        login:PropTypes.array
    };

    componentDidMount() {
        // const {is_members_loaded} = this.props.status;
        // const {statusGetMembersAction} = this.props;
        //
        // if(is_members_loaded === false){
        //     statusGetMembersAction();
        // }
        const {status, statusGetWinnersAction, login} = this.props;
        if(login.user.user_groups.season !== 0 && status.is_winners_loaded === false) {
            statusGetWinnersAction()
        }
    }

    render() {
        const {is_winners_loaded} = this.props.status;
        // const activated_at_array = this.props.login.user.user_groups[9].activation_time[0].activated_at;
        // const activated_at_array2 = this.props.login.user.user_groups.map(activate=>activate.activation_time);
        // // const activated_at_array3 = activated_at_array2.map(activate=>activate.activated_at);
        const current = this.props.login.current_date;
        const year = current.slice(0, 4);
        const month = current.slice(5, 7);
        const day = current.slice(8 ,10);
        const total_current_day = parseInt(year)*365 + parseInt(month)*30 + parseInt(day);
        console.log(total_current_day);
        // console.log(this.props.login.user.user_groups[9].activation_time.activated_at);
        return (
            is_winners_loaded === true ?
                <Fragment><StatusDetail/></Fragment>:
                <Fragment>

                </Fragment>
        );
    }
}

const mapStateToProps=(state)=>({
    status:state.status,
    login:state.login,
});
export default connect(mapStateToProps, {statusGetWinnersAction})(Status);