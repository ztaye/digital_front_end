import React,{Fragment} from 'react';
import './App.css';
import Header from "./components/Header";
import Dashboard from "./components/accounts/Dashbord";
import Alert from "./components/leads/Alerts";
import { transitions, positions, Provider as AlertProvider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';
import { Provider } from "react-redux";
import configureStore from "./components/redux/Store";
import {HashRouter as Router, Route, Switch, Redirect} from "react-router-dom";
import Register from "./components/accounts/Register"
import Login from "./components/accounts/Login";
import Join from "./components/join/Join"
import PrivateRoute from "./components/common/PrivateRoute"
import FirstPayment from "./components/join/FirstPayment";
import Group from "./components/join/Group";
import PaymentChoice from "./components/join/PaymentChoice";
import Status from "./components/status/Status";
import PaymentDashboard from "./components/pay/PaymentDashboard";
import PayManual from "./components/pay/PayManual";
import PayOnline from"./components/pay/PayOnline";
import FirstPayOnline from "./components/join/FirstPayOnline";
import AdminDashbord from "./components/admin/AdminDashbord";
import AddUser from "./components/admin/AddUser";
import Feedbacks from "./components/admin/Feedbacks";
import ManageGroups from "./components/admin/ManageGroups";
import Sale from "./components/sale_buy/Sale";
import Buy from "./components/sale_buy/Buy";
import SaleBuyPayManual from "./components/sale_buy/SaleBuyPayManual";
import SaleBuyPayOnline from "./components/sale_buy/SaleBuyPayOnline";
import AddPeriod from "./components/admin/AddPeriod";
import AddDuration from "./components/admin/AddDuration";
import AddAmount from "./components/admin/AddAmount";
import AddCategory from "./components/admin/AddCategory";
import Activities from "./components/admin/Activities";
import ManageUser from "./components/admin/ManageUser";
import Balance from "./components/admin/Balance";
import StatusBalance from "./components/status/StatusBalance";
import StatusFeedback from "./components/status/StatusFeedback";

const options = {
  // you can also just use 'bottom center'
  position: positions.TOP_RIGHT,
  timeout: 5000,
  offset: '30px',
  // you can also just use 'scale'
  transition: transitions.SCALE
};

function App() {
  return (
    <div className="App">

          <Provider store={configureStore}>
              <AlertProvider template={AlertTemplate} {...options}>
                  <Router>
                      <Fragment>
                          <Header/>
                            <Alert/>
                            <div className="container">
                                <Switch>
                                   <Route exact path="/signup" component={Register}/>
                                   <Route exact path="/login" component={Login}/>
                                   <PrivateRoute exact path="/" component={Dashboard}/>
                                   <PrivateRoute exact path="/join" component={Join}/>
                                   <PrivateRoute exact path="/join/first_payment" component={FirstPayment}/>
                                   <PrivateRoute exact path="/join/group" component={Group}/>
                                   <PrivateRoute exact path="/join/payment_choice" component={PaymentChoice}/>
                                   <PrivateRoute exact path="/status" component={Status}/>
                                   <PrivateRoute exact path="/payment" component={PaymentDashboard}/>
                                   <PrivateRoute exact path="/pay_manual" component={PayManual}/>
                                   <PrivateRoute exact path="/pay_online" component={PayOnline}/>
                                   <PrivateRoute exact path="/join/first_payment/online" component={FirstPayOnline}/>
                                   <PrivateRoute exact path="/admin" component={AdminDashbord}/>
                                   <PrivateRoute exact path="/admin/add_user" component={AddUser}/>
                                   <PrivateRoute exact path="/admin/feedback" component={Feedbacks}/>
                                   <PrivateRoute exact path="/admin/manage_groups" component={ManageGroups}/>
                                   <PrivateRoute exact path="/sale_buy/sale" component={Sale}/>
                                   <PrivateRoute exact path="/sale_buy/buy" component={Buy}/>
                                   <PrivateRoute exact path="/sale_buy/pay_manual" component={SaleBuyPayManual}/>
                                   <PrivateRoute exact path="/sale_buy/pay_online" component={SaleBuyPayOnline}/>
                                   <PrivateRoute exact path="/admin/add_period" component={AddPeriod}/>
                                   <PrivateRoute exact path="/admin/add_duration" component={AddDuration}/>
                                   <PrivateRoute exact path="/admin/add_amount" component={AddAmount}/>
                                   <PrivateRoute exact path="/admin/add_category" component={AddCategory}/>
                                   <PrivateRoute exact path="/admin/get_activities" component={Activities}/>
                                   <PrivateRoute exact path="/admin/manage_user" component={ManageUser}/>
                                   <PrivateRoute exact path="/admin/balance" component={Balance}/>
                                   <PrivateRoute exact path="/status/balance" component={StatusBalance}/>
                                   <PrivateRoute exact path="/status/feedback" component={StatusFeedback}/>
                                </Switch>
                            </div>
                      </Fragment>
                  </Router>
              </AlertProvider>
          </Provider>
    </div>
  );
}

export default App;
